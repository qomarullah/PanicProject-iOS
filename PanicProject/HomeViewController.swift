//
//  HomeViewController.swift
//  PanicProject
//
//  Created by qomarullah on 12/28/17.
//  Copyright © 2017 qomarullah. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AVFoundation

class HomeViewController : UIViewController{
    let STATUS_URL="http://159.89.200.55/puri/api/getStatusAlarm.php?"
    let SUBMIT_URL="http://159.89.200.55/puri/api/submit.php?"
    
    let defaultValues = UserDefaults.standard
    var status : Bool=false;
    
    let color0 = UIColor(rgb: 0x009933).cgColor
    let color1 = UIColor(rgb: 0xff0000).cgColor
    
    
    //var audioPlayer:AVAudioPlayer!

    
    @IBOutlet weak var btnPanic: UIButton!
    
    var player:AVAudioPlayer = AVAudioPlayer()
    
    
    @discardableResult func playSound(named soundName: String) -> AVAudioPlayer {
        
        
        let audioPath = Bundle.main.path(forResource: soundName, ofType: "wav")
        player = try! AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPath!) as URL)
        player.play()
        return player
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnPanic.layer.cornerRadius=130
        
        getStatus()
        
       //get Status
        let status = defaultValues.string(forKey: "status_alarm")
        
        if(status=="0"){
            btnPanic.setTitle("MATIKAN", for: UIControlState.normal)
            btnPanic.layer.backgroundColor=color0
        }else{
            btnPanic.setTitle("NYALAKAN", for: UIControlState.normal)
            btnPanic.layer.backgroundColor=color1
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getStatus()
        setButton()
    }
    func getStatus(){   //making a post request
        let userid=defaultValues.integer(forKey:"userid")
        let parameters: Parameters=[
            "userid":userid,
        ]
        Alamofire.request(STATUS_URL, method: .get, parameters:parameters).responseJSON
            {
                response in
                //printing response
                print(response)
                
                //getting the json value from the server
                if let result = response.result.value {
                let jsonData = result as! NSDictionary
                
                //if there is no error
                if(jsonData.value(forKey: "status") as! String=="success"){
                
                //getting user values
                let status_alarm = jsonData.value(forKey: "status_alarm") as! Int
                
                //saving user values to defaults
                self.defaultValues.set(status_alarm, forKey: "status_alarm")
            
                }else{
                    //error message in case of invalid credential
                    print("Error timeout get status")
                }
            }
        }
    }
                
    @IBAction func submitPanic(_ sender: Any) {
        
        
        //play sound
        
        playSound(named: "beep")
        /////////
        
        
        submitStatus()
        setButton()
        
        
        
    }
    
    func setButton(){
        let status = defaultValues.string(forKey: "status_alarm")
        
        if(status=="1"){
            btnPanic.setTitle("MATIKAN", for: UIControlState.normal)
            btnPanic.layer.backgroundColor=color0
        }else{
            btnPanic.setTitle("NYALAKAN", for: UIControlState.normal)
            btnPanic.layer.backgroundColor=color1
        }
    }
    
    func submitStatus(){   //making a post request
        
        let status = defaultValues.string(forKey: "status_alarm")
        
        let userid=defaultValues.integer(forKey:"userid")
        
        let parameters: Parameters=[
            "userid":userid,
            "status":status ?? "1"
            ]
        Alamofire.request(SUBMIT_URL, method: .get, parameters:parameters).responseJSON
            {
                response in
                //printing response
                print(response)
                
                //getting the json value from the server
                if let result = response.result.value {
                    let jsonData = result as! NSDictionary
                    
                    //if there is no error
                    if(jsonData.value(forKey: "status") as! String=="success"){
                        
                        //getting user values
                      
                        var set_status="1"
                        if(status=="1"){
                            set_status="0"
                        }else{
                            set_status="1"
                        }
         
                        self.defaultValues.set(set_status, forKey: "status_alarm")
                        self.defaultValues.synchronize()
                        
                        self.setButton()
                    }else{
                        //error message in case of invalid credential
                        print("Error timeout submit status")
                    }
                    
                   
                    
                }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
