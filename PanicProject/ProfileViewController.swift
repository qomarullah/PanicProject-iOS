//
//  AlertController.swift
//  PanicProject
//
//  Created by klikss on 1/2/18.
//  Copyright © 2018 qomarullah. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
class ProfileViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var titlehome: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phone: UILabel!
    
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnSimpan: UIButton!
    @IBOutlet weak var btnSecurity: UIButton!
    @IBOutlet weak var btnAdmin: UIButton!
    
    
    let defaultValues = UserDefaults.standard
    
    @IBOutlet weak var labelPassword: UILabel!
    @IBOutlet weak var Password1: UITextField!
    @IBOutlet weak var Password2: UITextField!
    let PASSWORD_URL="http://159.89.200.55/puri/api/newPassword.php?"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnAdmin.layer.cornerRadius=20
        btnSecurity.layer.cornerRadius=20
        btnLogout.layer.cornerRadius=10
        btnSimpan.layer.cornerRadius=10
        
        
        self.Password1.delegate = self;
        self.Password2.delegate = self;
        
        if let _username = defaultValues.string(forKey: "username"){
            username.text=_username
        }
        if let _title = defaultValues.string(forKey: "title"){
            titlehome.text=_title
        }
        if let _address = defaultValues.string(forKey: "address"){
            address.text=_address
        }
        if let _phone = defaultValues.string(forKey: "phone"){
            phone.text=_phone
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBAction func submitLogout(_ sender: Any) {
        defaultValues.removeObject(forKey:"username")
        //print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        self.dismiss(animated: false, completion: nil)
        let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.present(loginViewController, animated: false, completion: nil)
    }
    @IBAction func submitPassword(_ sender: Any) {
        
        let userid=defaultValues.integer(forKey:"userid")
        let _password1:String=Password1.text!
        let _password2:String=Password2.text!
        if(_password1==_password2 && _password1=="" && _password2==""){
            labelPassword.text="Pastikan terisi benar"
            return
        }
        let parameters: Parameters=[
            "userid":userid,
            "password":_password1            ]
        Alamofire.request(PASSWORD_URL, method: .get, parameters:parameters).responseJSON
            {
                response in
                //printing response
                print(response)
                
                //getting the json value from the server
                if let result = response.result.value {
                    let jsonData = result as! NSDictionary
                    
                    //if there is no error
                    if(jsonData.value(forKey: "status") as! String=="success"){
                        self.defaultValues.removeObject(forKey:"username")
                        
                        let alertController = UIAlertController(title: "Sukses", message: "Apakah mau logout untuk coba password baru?", preferredStyle: .alert)
                        let yesPressed = UIAlertAction(title: "Ya", style: .default, handler: { (action) in
                            
                            self.dismiss(animated: false, completion: nil)
                            let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(loginViewController, animated: false, completion: nil)
                            
                        })
                        let noPressed = UIAlertAction(title: "Tidak", style: .default, handler: { (action) in
                            
                        })
                        alertController.addAction(yesPressed)
                        alertController.addAction(noPressed)
                        self.present(alertController, animated: true, completion: nil)
                        
                    }else{
                        //error message in case of invalid credential
                        //print("Error timeout submit status")
                        let alertController = UIAlertController(title: "Gagal", message: "Pastikan koneksi internet tersedia.", preferredStyle: .alert)
                        let yesPressed = UIAlertAction(title: "Tutup", style: .default, handler: { (action) in
                            
                        })
                        alertController.addAction(yesPressed)
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                    }
                    
            }
        }
    }
    @IBAction func callSecurity(_ sender: Any) {
        
        let contact=defaultValues.string(forKey: "admin") ?? "not found"
        
        let url: NSURL = URL(string: "tel://\(contact)")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        
    }
    
    @IBAction func callAdmin(_ sender: Any) {
    
        let contact=defaultValues.string(forKey: "security") ?? "not found"
        
        let url: NSURL = URL(string: "tel://\(contact)")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
}

}
