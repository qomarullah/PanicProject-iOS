//
//  ViewController.swift
//  PanicProject
//
//  Created by qomarullah on 12/27/17.
//  Copyright © 2017 qomarullah. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var Username: UITextField!
    
    @IBOutlet weak var Password: UITextField!
    
    @IBOutlet weak var MsgLogin: UILabel!
   
    let LOGIN_URL="http://159.89.200.55/puri/api/login.php?"
    
    let CONTACT_URL="http://159.89.200.55/puri/api/contact.php?"
    
    let defaultValues = UserDefaults.standard
    
    @IBAction func callAdmin(_ sender: Any) {
        
        let parameters: Parameters=[
            "userid":"userid"]
        Alamofire.request(CONTACT_URL, method: .get, parameters:parameters).responseJSON
            {
                response in
                //printing response
                print(response)
                
                //getting the json value from the server
                if let result = response.result.value {
                    let jsonData = result as! NSDictionary
                    
                    //if there is no error
                    if(jsonData.value(forKey: "status") as! String=="success"){
                        
                        let contactAdmin = jsonData.value(forKey: "admin") as! String
                        
                        let url: NSURL = URL(string: "tel://\(contactAdmin)")! as NSURL
                        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                        
                    }else{
                        //error message in case of invalid credential
                        //print("Error timeout submit status")
                        let alertController = UIAlertController(title: "Gagal", message: "Pastikan koneksi internet tersedia.", preferredStyle: .alert)
                        let yesPressed = UIAlertAction(title: "Tutup", style: .default, handler: { (action) in
                            
                        })
                        alertController.addAction(yesPressed)
                        self.present(alertController, animated: true, completion: nil)
                        
                        
                    }
                    
                }
        }
    
        
    }
    
    @IBAction func submitLogin(_ sender: Any) {
        let username:String=Username.text!
        let password:String=Password.text!
        print(username)
        print(password)
        
        let parameters: Parameters=[
            "username":Username.text!,
            "password":Password.text!
        ]
        //making a post request
        Alamofire.request(LOGIN_URL, method: .get, parameters:parameters).responseJSON
            {
                response in
                //printing response
                print(response)
                
                //getting the json value from the server
                if let result = response.result.value {
                    let jsonData = result as! NSDictionary
                    
                    //if there is no error
                    if(jsonData.value(forKey: "status") as! String=="success"){
                        
                        //getting the user from response
                        //let user = jsonData.value(forKey: "user") as! NSDictionary
                        
                        //getting user values
                        let userid = jsonData.value(forKey: "userid") as! Int
                        let username = jsonData.value(forKey: "username") as! String
                        let title = jsonData.value(forKey: "title") as! String
                        let address = jsonData.value(forKey: "address") as! String
                        let phone = jsonData.value(forKey: "phone") as! Int
                        
                        //saving user values to defaults
                        self.defaultValues.set(userid, forKey: "userid")
                        self.defaultValues.set(username, forKey: "username")
                        self.defaultValues.set(title, forKey: "title")
                        self.defaultValues.set(address, forKey: "address")
                        self.defaultValues.set(phone, forKey: "phone")
                        
                        //switching the screen
                      
                        self.dismiss(animated: false, completion: nil)
                        let tabViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
                        tabViewController.selectedViewController = tabViewController.viewControllers?[0]
                        self.present(tabViewController, animated: false, completion: nil)
                        
                        
                        //self.MsgLogin.text="Login Success"
                    }else{
                        //error message in case of invalid credential
                        self.MsgLogin.text = "Invalid username or password"
                    }
                }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }


    
    override func viewDidLoad() {
        super.viewDidLoad()
         btnLogin.layer.cornerRadius=20
         self.Username.delegate = self;
         self.Password.delegate = self;
        
    }

    override func viewDidAppear(_ animated: Bool) {
       
        if defaultValues.string(forKey: "username") != nil{
            self.dismiss(animated: false, completion: nil)
            
            let tabViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
            tabViewController.selectedViewController = tabViewController.viewControllers?[0]
            self.present(tabViewController, animated: false, completion: nil)
            
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

