//
//  HistoryViewController.swift
//  PanicProject
//
//  Created by qomarullah on 12/28/17.
//  Copyright © 2017 qomarullah. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    
    let HISTORY_URL="http://159.89.200.55/puri/api/getHistory.php?a=1"
   // let HISTORY_URL="http://www.thecodeeasy.com/test/aswiftjson.json?"
    let defaultValues = UserDefaults.standard
    
    var historyArray=[AnyObject]()
    
    
    @IBOutlet weak var tableView: UITableView!
    private var data: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        getHistory()
        
        
        /*for i in 0...1000 {
            data.append("\(i)")
        }
      */
        
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func getHistory(){   //making a post request
        let userid=defaultValues.integer(forKey:"userid")
        let parameters: Parameters=[
            "userid":userid,
            ]
        print(HISTORY_URL)
        print(userid)
        Alamofire.request(HISTORY_URL, method: .get, parameters:parameters).responseJSON
            {
                
                response in
                //printing response
                //print(response)
                
                let result = response.result
                if let dict = result.value as? Dictionary<String, AnyObject>
                {
                    //print("page")
                    //print(dict["page"] ?? "test")
                    
                    let contactAdmin=dict["admin"]
                    let contactSecurity=dict["security"]
                    
                    self.defaultValues.set(contactAdmin, forKey: "admin")
                    self.defaultValues.set(contactSecurity, forKey: "security")

                    if let innerDict=dict["results"]
                    {
                        //print("ok")
                        self.historyArray = innerDict as! [AnyObject]
                        self.tableView.reloadData()
                    }
                }
                
                
                
                
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        getHistory()
        
    }
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    print(historyArray.count)
    
    return historyArray.count
    
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CustomTableViewCell
        self.tableView.rowHeight = 100

        let start = historyArray[indexPath.row]["created_time"] as! String
        let finish = historyArray[indexPath.row]["finish_time"] as! String
        let progress = historyArray[indexPath.row]["progress_time"] as! String
        
        //if(finish != nil){ finish = "-"}
        
        cell?.titleLabel.text = "Start   : " + start
        cell?.progressLabel.text = "Action : " + progress
        cell?.finishLabel.text = "End     : " + finish
        
        return cell!
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
